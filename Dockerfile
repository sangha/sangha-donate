FROM alpine

RUN apk add --no-cache openssl nginx

ENV DOCKERIZE_VERSION v0.6.0
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

RUN echo "daemon off;" >> /etc/nginx/nginx.conf \
  && mkdir "/run/nginx"
EXPOSE 80

ADD docs/nginx.template.conf /etc/nginx/conf.d/default.tmpl
COPY build /var/www/donate
ARG RELEASE_SHA
ARG RELEASE_NAME
ENV RELEASE_SHA $RELEASE_SHA
ENV RELEASE_NAME $RELEASE_NAME

CMD ["dockerize", "-template", "/etc/nginx/conf.d/default.tmpl:/etc/nginx/conf.d/default.conf", "-stdout", "/var/log/nginx/access.log", "-stderr", "/var/log/nginx/error.log", "-poll", "nginx"]
