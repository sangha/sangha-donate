# Polymer Sangha donation frontend

All commands are in the script section of the [package.json](package.json), nodejs and npm should be installed.

You can run one of the prebuilt docker images with `docker run -p 8080:80 -e API_URL=https://sandbox.api.sangha.techcultivation.org/v1 registry.gitlab.com/sangha/sangha-donate`.

## Development

```bash
# get dependencies
npm install

# edit index.html to match your api url

# serve on localhost:8081
npm start

# get new web-component
npm run wcinstall paper-checkbox
```

## Build with docker

```bash
npm run build && docker build -t sangha-donate .
```

## Customize the look and feel

This project is based on the material style elements by Google to fit the [techcultivation](https://techcultivation.org) design it ships with a modified version of [paper-styles](https://github.com/PolymerElements/paper-styles) the [techcultivation-paper-styles](https://gitlab.com/sangha-webcomponents/techcultivation-paper-styles). To get the default material design just overwrite the [bower file](bower.json) and set `--primary-color` to your own needs.

## Error logging with sentry

```bash
# generate secret key (change environment in docker-compose)
docker-compose -f docs/docker-compose-sentry.yml exec sentry sentry config generate-secret-key

# start with docker compose
docker-compose -f docs/docker-compose-sentry.yml up

# generate config and initial user
docker-compose -f docs/docker-compose-sentry.yml exec sentry sentry upgrade

# set sentry_dsn in index.html accordingly
```
